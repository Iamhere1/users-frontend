import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import axios from "axios";

const useStyles = makeStyles(theme => ({
  container: {
    margin: "0 400px",
    display: "flex",
    alignItems: "center",
    flexDirection: "column"
  },
  root: {
    marginTop: "10px",
    width: "100%",
    maxWidth: 380
  },
  inline: {
    display: "inline"
  }
}));

function useDebounce(value, delay) {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}

export default function App() {
  const BASE_URL = 'http://localhost:3000'
  const classes = useStyles();
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");
  const searchDebounced = useDebounce(search, 300);
  useEffect(() => {
    // don't show spinner if loading < 500ms
    const loadingTimeout = setTimeout(() => {
      setLoading(true);
    }, 500);
    axios
      .get(`${BASE_URL}/users?q=${searchDebounced}`)
      .then(({ data }) => {
        setUsers(data);
        clearTimeout(loadingTimeout);
        setLoading(false);
      })
      .catch(e => {
        clearTimeout(loadingTimeout);
        setLoading(false);
      });
    return () => {
      clearTimeout(loadingTimeout);
    };
  }, [searchDebounced]);

  return (
    <div className={classes.container}>
      <TextField
        variant="outlined"
        fullWidth
        onChange={e => setSearch(e.target.value)}
        value={search}
      />
      {loading ? (
        <CircularProgress />
      ) : (
        <List className={classes.root}>
          {users.map(user => (
            <>
              <ListItem alignItems="flex-start">
                <ListItemAvatar>
                  <Avatar
                    alt={user.firstName + " " + user.lastName}
                    src={user.avatar}
                  />
                </ListItemAvatar>
                <ListItemText
                  primary={user.firstName + " " + user.lastName}
                  secondary={
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary"
                    >
                      <br />
                      {user.email}
                    </Typography>
                  }
                />
              </ListItem>
              <Divider variant="inset" component="li" />
            </>
          ))}
        </List>
      )}
    </div>
  );
}
